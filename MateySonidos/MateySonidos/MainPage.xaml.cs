﻿using System;
using MateySonidos.Utils;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MateySonidos
{
   [DesignTimeVisible(true)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            lblTitulo.VerticalTextAlignment = TextAlignment.Center;
            lblTitulo.HorizontalTextAlignment = TextAlignment.Center;
        }

        //*************************************************************************************************************************************
        //*************************************************************************************************************************************
        //METODOS
        //*************************************************************************************************************************************
        //*************************************************************************************************************************************
        private void ClickBtnIniciar(object sender, EventArgs e)
        {
            //lblTitulo.Text = "AlgeRA";

            //this.Navigation.PushModalAsync(new VisorRAPage());

            DependencyService.Get<ILaunchActivityOrViewController>().Launch("");

            ////Android
            //if (Device.RuntimePlatform == Device.Android)
            //{


            //    //iOS
            //}else if (Device.RuntimePlatform == Device.iOS)
            //{

            //}
        }


        //public MainPage()
        //{
        //    InitializeComponent();
        //}
    }
}
