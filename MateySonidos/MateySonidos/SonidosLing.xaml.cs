﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MateySonidos.Modelos;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MateySonidos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SonidosLing : ContentPage
    {
    
        private static bool banClick;
        public SonidosLing()
        {
            InitializeComponent();
            Title = "Sonidos de Ling";

            banClick = true;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ListaOpciones opciones = new ListaOpciones();

            lvLista.ItemsSource = opciones._listaopciones;
            lvLista.ItemSelected += LvLista_ItemSelected;

        }

         
        private async void LvLista_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (banClick)
            {
                var item = e.SelectedItem as ListaSonidosLing;

                if (item != null)
                {
                    banClick = false;

                    switch (item.Opcion)
                    {
                        case "Descripción del Test propuesto":
                            {
                                //Carga nueva pagina 
                                //await Application.Current.MainPage.Navigation.PushAsync(new SonidosLingPage());
                                await Navigation.PushAsync(new SonidosLingPage());
                                //IsPresented = false;
                            }
                            break;
                      
                        case "Test de Ling":

                            //await Application.Current.MainPage.Navigation.PushAsync(new SonidosLingIndivPage());
                            await Navigation.PushAsync(new SonidosLingIndivPage());

                            break;
                        
                    }

                    await Task.Run(async () =>
                    {
                        await Task.Delay(500);
                        banClick = true;
                    });

                }
            }
        }
    }
}