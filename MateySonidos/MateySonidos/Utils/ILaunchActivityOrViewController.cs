﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MateySonidos.Utils
{
    public interface ILaunchActivityOrViewController
    {
        void Launch(string nombreModelo);
    }
}
